import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

public class AutoCompletionTrie {

    int totalFreq = 0;
     static StringBuilder outputToFile = new StringBuilder();

    public Trie addWordsToTrie(String csvFile) throws IOException {
        DictionaryFinder df = new DictionaryFinder();
        ArrayList<String> in = df.readWordsFromCSV(csvFile);
        //DO STUFF TO df HERE in countFrequencies
        TreeMap<String, Integer> d = df.formDictionary(in);
        Trie trie = new Trie();

        for (Map.Entry<String, Integer> entry : d.entrySet()) {
            String key = entry.getKey();
            trie.add((String) key);
        }
        return trie;
    }

    public void loadPrefixQuery() throws IOException {
        Trie trie = addWordsToTrie("TextFiles/lotr.csv"); //lotr trie
        ArrayList<String> prefixs = makePrefixList("TextFiles/lotrQueries.csv"); //list of prefix

        ArrayList<String> in = DictionaryFinder.readWordsFromCSV("TextFiles/lotr.csv");
        TreeMap<String,Integer> dictionary = DictionaryFinder.formDictionary(in); //lotr dictionary

        TreeMap<String,Integer> matches;
        TreeMap<String,Double> result;
        ArrayList wordList;

        for (String pref : prefixs) {
            TrieNode tn = trie.getSubTrie(pref);
            totalFreq = 0;

            outputToFile.append(pref);
            if (tn != null) {
                Trie subTrie = new Trie(tn);
                List list = subTrie.getAllWords();
                wordList = makeWordList(pref,list); //add prefix to each word
                matches = matchedWords(dictionary,wordList);
                result = countProbabilities(matches);
                outputFirstThree(result);

                outputToFile.append("\n");
            }
        }
        saveToFile("lotrMatches.csv");
    }

    public static void saveToFile(String file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        printWriter.println(outputToFile.toString());

        printWriter.close();
    }

    public TreeMap<String,Double> countProbabilities(TreeMap<String,Integer> d) {
        List<Map.Entry<String, Integer>> listOfEntries = new ArrayList<>(d.entrySet());

        TreeMap<String,Double> wordAndProb = new TreeMap<>();
        for(Map.Entry<String, Integer> entry : listOfEntries) {
            String word = entry.getKey();
            double freq = entry.getValue();
            double probability = (freq/ (double) totalFreq);
            wordAndProb.put(word,probability);
        }

        return wordAndProb;
    }

    public ArrayList makeWordList(String pref, List list) {
        ArrayList wordList = new ArrayList();
        wordList.add(pref);
        for (int i = 0; i < list.size(); i++) {
            String word = pref + list.get(i);
            wordList.add(word);
        }
        return wordList;
    }

    public TreeMap<String,Integer> matchedWords(TreeMap<String,Integer> dictionary, List<String> list) {
        TreeMap<String,Integer> matches = new TreeMap<>();
        for (String str: list) {
            if (dictionary.containsKey(str)) {
                int freq = dictionary.get(str);
                matches.put(str,freq);
                totalFreq = totalFreq + freq;
            }
        }

        return matches;
    }

    public void outputFirstThree(TreeMap<String,Double> d) {
        List<Map.Entry<String, Double>> listOfEntries = new ArrayList(d.entrySet());

        Collections.sort(listOfEntries,sortByValue);
        Collections.reverse(listOfEntries);
        int count = 0;

        DecimalFormat df = new DecimalFormat("#.###");

        for(Map.Entry<String, Double> entry : listOfEntries) {
            String word = (String) entry.getKey();
            Double prob = entry.getValue();
            if (count < 3) {
                System.out.println(word + " (probability " + df.format(prob) + ")");
                outputToFile.append("," + word + "," + df.format(prob));

            }
            count++;
        }
        }

    public ArrayList makePrefixList(String csvFile) throws IOException {
        FileReader file = new FileReader(csvFile);
        BufferedReader br = new BufferedReader(file);
        String line = br.readLine();

        ArrayList<String> prefixs = new ArrayList<>();

        while (line != null) {
            prefixs.add(line);

            line = br.readLine();
        }

        return prefixs;
    }

    Comparator<Map.Entry<String, Double>> sortByValue = Map.Entry.comparingByValue();

    public static void main(String[] args) throws IOException {
        AutoCompletionTrie act = new AutoCompletionTrie();
        Trie t = act.addWordsToTrie("TextFiles/lotr.csv");
        act.loadPrefixQuery();
    }

}
