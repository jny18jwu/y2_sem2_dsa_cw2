import java.util.Arrays;

public class TrieNode {
    boolean isKey;
    TrieNode[] offspring;
    static int MAX_OFFSPRING = 26;
    Character letter;

    public TrieNode() {
        offspring = new TrieNode[MAX_OFFSPRING];
        letter = 0;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public static int getMaxOffspring() {
        return MAX_OFFSPRING;
    }

    @Override
    public String toString() {
        return "TrieNode{" +
                "offspring=" + Arrays.toString(offspring) +
                '}';
    }

}
